# GitLab CI/CD for Yarntown

The Yarntown GitLab CI/CD automatically packages Yarntown into a `.solarus` quest
archive and automatically creates packages bundled with a lightly customised copy
of the Solarus engine built for Windows 32/64-bit.

For every **commit** to any branch (e.g. `master`), the following is created:

* A `yarntown-GITHASH.solarus` quest archive.
* A `yarntown-GITHASH-64bit.zip` package bundle for 64-bit Windows.
* No 32-bit package bundle is created to save pipeline execution times.

For every release in the form of a **tag**, the following is created:

* A `yarntown-TAG.solarus` quest archive.
* A `yarntown-TAG-32bit.zip` package bundle for 32-bit Windows.
* A `yarntown-TAG-64bit.zip` package bundle for 64-bit Windows.

> **Note:** All artifacts will be deleted after 4 weeks of creation except for the
artifacts in the **latest** successful pipelines. This ensures only the latest artifacts
are persisted for each branch or tag without polluting the repository storage.

## Useful Links

Latest development downloads (`master` branch):

    https://gitlab.com/maxmraz/yarntown/-/jobs/artifacts/master/browse?job=quest-package
    https://gitlab.com/maxmraz/yarntown/-/jobs/artifacts/master/browse?job=quest-64bit-bundle

Release downloads (replace `TAG` with a concrete release tag):

    https://gitlab.com/maxmraz/yarntown/-/jobs/artifacts/TAG/browse?job=quest-package
    https://gitlab.com/maxmraz/yarntown/-/jobs/artifacts/TAG/browse?job=quest-32bit-bundle
    https://gitlab.com/maxmraz/yarntown/-/jobs/artifacts/TAG/browse?job=quest-64bit-bundle

Release direct downloads (replace `TAG` with a concrete release tag):

    https://gitlab.com/maxmraz/yarntown/-/jobs/artifacts/TAG/raw/yarntown-TAG.solarus?job=quest-package
    https://gitlab.com/maxmraz/yarntown/-/jobs/artifacts/TAG/raw/yarntown-TAG-32bit.zip?job=quest-32bit-bundle
    https://gitlab.com/maxmraz/yarntown/-/jobs/artifacts/TAG/raw/yarntown-TAG-64bit.zip?job=quest-64bit-bundle

